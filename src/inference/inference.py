from ultralytics import YOLO

# # Configure the tracking parameters and run the tracker
model = YOLO('models/best.pt') # 'models/onnx.pt'
source_url = 'https://www.youtube.com/watch?v=DGoQzuuL5CA&pp=ygUY0LXQtNGDINC_0L4g0LTQvtGA0L7Qs9C1'
results = model.track(source=source_url, conf=0.1, iou=0.5, show=True) #imgsz=(1280, 736) as parameter for .onnx

count_frame = 0
total_time = 0
for i in results:
    count_frame += 1
    total_time += i.speed['inference']

print(f"FPS: {(count_frame / total_time) * 1000}")

# results = model.predict(source_url, stream=True, show=True, imgsz=1280)#, imgsz=1280, conf=0.3, iou=0.5)
