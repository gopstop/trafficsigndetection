import os
from typing import Any, Dict

import yaml
from ultralytics import YOLO


def load_config(config_path: str) -> Dict[str, Any]:
    """
    Загружает конфигурацию из YAML файла.

    Args:
        config_path (str): Путь к файлу конфигурации.

    Returns:
        dict: Словарь с загруженными данными из конфигурационного файла.
    """
    with open(config_path, 'r') as file:
        return yaml.safe_load(file)


def train_yolo(config: Dict[str, Any]) -> None:
    """
    Обучает модель YOLO на основе переданной конфигурации.

    Args:
        config (dict): Конфигурация обучения, включая параметры и
                       аргументы для обучения модели.
    """
    # Установка переменных окружения
    os.environ['WANDB_DISABLED'] = str(config['environment']['wandb_disabled'])

    # Инициализация модели
    model = YOLO('yolov8m.pt')

    # Подготовка аргументов для обучения
    train_args = config['training']

    # Обучение модели
    model.train(**train_args)


if __name__ == "__main__":
    config = load_config('../configs/training_conf.yaml')
    train_yolo(config)
