import json

import pandas as pd


def get_df_annotations(annotations_file: str, 
                       label_map_file: str,
                       min_area: int) -> pd.DataFrame:
    """
    Загружает данные аннотаций из JSON файла и соотносит их с метками из
    файла label map.

    Args:
        annotations_file (str): Путь к JSON файлу с аннотациями.
        label_map_file (str): Путь к JSON файлу с картой меток.
        min_area (int): Минимальный размер баундинг бокса.

    Returns:
        pd.DataFrame: DataFrame, содержащий аннотации с дополнительными
        столбцами 'sign_name' и 'global_group'.
    """
    with open(annotations_file, 'r', encoding='utf-8') as file:
        json_data = json.load(file)

    with open(label_map_file) as f:
        label_map = json.load(f)

    id2label = {v: k for k, v in label_map.items()}

    df = pd.DataFrame(json_data['annotations'])
    df['sign_name'] = df['category_id'].map(id2label)
    df['global_group'] = df['sign_name'].apply(lambda x: x.split('_')[0])
    df = df[df['area'] >= min_area]

    return df


def get_balanced_df(df: pd.DataFrame, samples_per_class: int) -> pd.DataFrame:
    """
    Создает сбалансированный DataFrame, выбирая заданное количество образцов
    для каждого класса.

    Args:
        df (pd.DataFrame): DataFrame, содержащий данные для балансировки.
        samples_per_class (int): Количество образцов для каждого класса.

    Returns:
        pd.DataFrame: Сбалансированный DataFrame.
    """
    balanced_data = []

    for class_id in df['category_id'].unique():
        class_data = df[df['category_id'] == class_id]
        sample = class_data.sample(min(samples_per_class, len(class_data)),
                                   replace=False,
                                  random_state=42)
        balanced_data.append(sample)

    balanced_df = pd.concat(balanced_data)
    return balanced_df


def get_filtered_ids(coco_json_train: str,
                     label_map_file: str,
                     samples_per_class: int,
                     min_area: int = 0) -> list:
    """
    Генерирует список идентификаторов из обучающего набора данных COCO,
    сбалансированный по классам.

    Args:
        coco_json_train (str): Путь к JSON файлу обучающего набора данных COCO.
        label_map_file (str): Путь к JSON файлу с картой меток.
        samples_per_class (int): Количество образцов для каждого класса.

    Returns:
        list: Список идентификаторов, сбалансированный по классам.
    """
    df_anno = get_df_annotations(coco_json_train, label_map_file, min_area)

    print(len(df_anno['sign_name'].unique()))
    samples_per_class = 100
    balanced_df = get_balanced_df(df_anno, samples_per_class)

    return balanced_df['id'].to_list()
