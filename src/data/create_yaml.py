import yaml


def create_yaml_dataset(path_yolo_dataset: str, labels_path: str, only_one_class: bool):
    """
    Создает файл конфигурации YAML для набора данных YOLO.

    Args:
        path_yolo_dataset (str): Путь к директории набора данных YOLO,
        где будет создан конфигурационный файл.
        labels_path (str): Путь к файлу, содержащему метки классов.
        only_one_class (bool): Если True, в файле конфигурации будет указан
                               только один класс 'traffic_sign'.
                               Если False, классы будут загружены из файла,
                               указанного в `labels_path`.

    Эта функция создает файл YAML, который указывает пути к обучающим и валидационным
    изображениям, а также имена классов, используемые в обучении модели YOLO.
    """
    data = {
        'path': path_yolo_dataset,
        'train': 'train/images',
        'val': 'valid/images',
    }

    if only_one_class:
        data['names'] = {
            0: 'traffic_sign'
        }
    else:
        labels_path = labels_path
        with open(labels_path, 'r') as file:
            class_names = [line.strip() for line in file]
            id2class = dict(zip(range(len(class_names)), class_names))
            data['names'] = id2class

    with open(f'{path_yolo_dataset}/traffic_signs.yaml', 'w') as file:
        yaml.dump(data, file)
