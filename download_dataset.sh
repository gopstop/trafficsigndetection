#!/bin/bash

# Загрузка файла kaggle.json
gdown --id 1BOdGqui7Sv1JjmaS9D09cOFbUttm1zVP

# Создание директории ~/.kaggle
mkdir ~/.kaggle

# Перемещение kaggle.json в ~/.kaggle/
mv kaggle.json ~/.kaggle/

# Установка прав доступа к файлу kaggle.json
chmod 600 ~/.kaggle/kaggle.json

# Скачивание датасета с Kaggle
kaggle datasets download -d watchman/rtsd-dataset

# Распаковка скачанного архива
unzip -q rtsd-dataset.zip

# Удаление скачанного архива
rm -rf rtsd-dataset.zip
