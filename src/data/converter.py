import json
import os
import shutil

from tqdm import tqdm


def convert_coco_to_yolo(coco_json: str,
                         output_dir: str,
                         image_dir: str,
                         filter_anno: set = None,
                         only_one_class: bool = False):
    """
    Функция создает необходимые папки в output_dir, конвертирует аннотации COCO в формат
    YOLO и копирует соответствующие изображения.

    Args:
        coco_json (str): Путь к JSON файлу с аннотациями COCO.
        output_dir (str): Путь к выходной директории, где будут сохранены аннотации и
        изображения YOLO.
        image_dir (str): Путь к директории, содержащей исходные изображения.
        filter_anno (set, optional): Набор идентификаторов аннотаций для включения в
        обработку. Если None, обрабатываются все аннотации. Defaults to None.
        only_one_class (bool, optional): Если True, все объекты будут принадлежать к
        одному классу (0). Defaults to False.

    """
    # создаем нужные папки, если их нет
    os.makedirs(output_dir, exist_ok=True)
    os.makedirs(os.path.join(output_dir, 'labels'), exist_ok=True)
    os.makedirs(os.path.join(output_dir, 'images'), exist_ok=True)

    # Загрузка данных COCO
    with open(coco_json) as f:
        data = json.load(f)

    # Создание словаря для быстрого доступа к данным изображений
    images_info = {image['id']: image for image in data['images']}

    # Конвертация аннотаций в формат YOLO
    for ann in tqdm(data['annotations']):
        if filter_anno:
            if ann['id'] not in filter_anno:
                continue

        image_info = images_info[ann['image_id']]
        image_file_name = image_info['file_name'].split('/')[1]
        path_to_image = os.path.join(image_dir, image_file_name)

        # Проверка наличия файла изображения
        if not os.path.exists(path_to_image):
            print(image_file_name)
            continue

        # Установка единого class_id для всех знаков
        if only_one_class:
            cat_id = 0
        else:
            cat_id = ann['category_id'] - 1

        # Координаты баундинг бокса
        width, height = image_info['width'], image_info['height']
        x_center = (ann['bbox'][0] + ann['bbox'][2] / 2) / width
        y_center = (ann['bbox'][1] + ann['bbox'][3] / 2) / height
        bbox_width = ann['bbox'][2] / width
        bbox_height = ann['bbox'][3] / height

        # Создание строки аннотации в формате YOLO
        yolo_format = f"{cat_id} {x_center} {y_center} {bbox_width} {bbox_height}\n"

        # задаем пути для сохранения аннотации и изображения
        label_file_name = os.path.splitext(image_file_name)[0] + '.txt'
        labels_output_path = os.path.join(output_dir, 'labels', label_file_name)
        images_output_path = os.path.join(output_dir, 'images', image_file_name)

        # Запись аннотации в соответствующий файл
        with open(labels_output_path, 'a') as file:
            file.write(yolo_format)

        # копируем файл в нужную папку
        shutil.copy(path_to_image, images_output_path)
