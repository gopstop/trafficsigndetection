import argparse
from typing import Any, Dict

import yaml
from converter import convert_coco_to_yolo
from create_yaml import create_yaml_dataset
from filter_ids import get_filtered_ids


def get_args_parser() -> argparse.Namespace:
    """
    Парсит аргументы командной строки.

    Returns:
        argparse.Namespace: Объект, содержащий аргументы в виде атрибутов.
    """
    parser = argparse.ArgumentParser(description="Training")
    parser.add_argument("-config_path",
                        default='src/configs/default_config.yaml',
                        type=str,
                        help="Path to the configuration file")
    return parser.parse_args()


def get_dataset(config: Dict[str, Any]) -> None:
    """
    Создает YOLO-совместимый набор данных на основе переданной конфигурации.

    Args:
        config (dict): Конфигурация создания набора данных,
                       включая параметры и аргументы.
    """
    filter_annotation_id = get_filtered_ids(
                                coco_json_train=config['coco_json']['train'],
                                label_map_file=config['path_label_map'],
                                samples_per_class=config['samples_per_class'],
                                min_area=config['min_area'])

    convert_coco_to_yolo(coco_json=config['coco_json']['train'],
                         output_dir=config['path_yolo_dataset'] + '/train',
                         image_dir=config['path_to_images'],
                         filter_anno=filter_annotation_id,
                         only_one_class=config['train_single_class_only'])

    convert_coco_to_yolo(coco_json=config['coco_json']['val'],
                         output_dir=config['path_yolo_dataset'] + '/valid',
                         image_dir=config['path_to_images'],
                         only_one_class=config['train_single_class_only'])

    create_yaml_dataset(config['path_yolo_dataset'],
                        labels_path=config['path_labels'],
                        only_one_class=config['train_single_class_only'])


if __name__ == "__main__":
    args = get_args_parser()
    try:
        with open(args.config_path, 'r') as file:
            config = yaml.safe_load(file)
    except Exception as e:
        print(f"Ошибка при чтении файла конфигурации: {e}")
        exit(1)

    get_dataset(config)
